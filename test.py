import sys
import re
from urllib.parse import urlparse
import requests
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as bs


try:
    root_link = sys.argv[1]
except IndexError:
    print("Usage: python3 test.py http://example.com")
    sys.exit(0)


ua = UserAgent()
domain = urlparse(root_link).netloc
useragent = {'User-Agent': ua.chrome}

session = requests.Session()
session.headers.update(useragent)
response = session.get(root_link)


def get_links(response):
    soup = bs(response.text, "html.parser")
    links = [a.get('href') for a in soup.find_all('a', href=True)]
    links = list(set(links))
    return links


def get_requests(links):
    for link in links:
        if urlparse(link).netloc == domain:
            response = session.get(link)
            if response.ok:
                yield response


def filter_links(start_page, links):
    links = [i if i.startswith('http')
             else start_page + "/" + i for i in links]
    links = [re.sub(r'[^:](\/\/)', r'/', i) for i in links]
    links = [i for i in links if i.startswith(start_page) and i != start_page]
    links = list(set(links))
    return links


links = get_links(response)
links = filter_links(root_link, links)
result_links = []
for response in get_requests(links):
    result_links += get_links(response)
links = filter_links(root_link, result_links)

for response in get_requests(links):
    soup = bs(response.text, "html.parser")
    print(response, soup.find_all('form', href=True))

# print(links)
